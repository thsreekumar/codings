package org.sreekumar.factorial;

public class Factorial {
	static void fact(int n)
	{
		int f = 1;
		if(n == 0)
			System.out.print("Factorial : 1 ");
		else
		{
			for (int i=1; i<=n; i++)
			{
				f = f * i;	
			}
			System.out.print("Factorial : " + f);
		}

	}
	public static void main(String args[])
	{
		int n = 5;
		fact(n);
	}
}