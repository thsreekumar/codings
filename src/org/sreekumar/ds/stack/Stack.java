package org.sreekumar.ds.stack;
/*Stack program to push,pop and display methods are used*/
public class Stack {
	static int MAX = 1000;
	int top;
	int array[] = new int[MAX];
	/*push method is to insert element to the stack*/
	boolean push(int number)
	{
		if(top >= (MAX - 1))
		{
			System.out.println("Stack overflow!!!");
			return false;
		}
		else
		{
			array[top] = number;
			top++;
			return true;	
		}
	}
	/*pop method to delete an element from stack*/
	int pop()
	{
		if(top < 0)
		{
			System.out.println("Stack underflow");
			return 0;
		}
		else
		{
			top--;
			return array[top];

		}
	}
	/*display method to display the stack*/
	void display()
	{
		if(top < 0)
		{
			System.out.println("Stack is empty");
		}
		else
		{
			for(int i = 0; i < top; i++ )
			{
				System.out.println(array[i]+" ");
			}
		}
	}
	public static void main(String args[])
	{
		Stack stk = new Stack();
		stk.push(1);
		stk.push(2);
		stk.push(3);
		System.out.println("Array is : ");
		stk.display();
		System.out.println();
		System.out.println(stk.pop()+ " Removed from Stack");
	}
}
