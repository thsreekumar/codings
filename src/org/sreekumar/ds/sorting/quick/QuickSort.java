package org.sreekumar.ds.sorting.quick;

import org.sreekumar.common.utils.Utils;

class QuickSort {

	public void sort(int[] a) {

		sort(a, 0, a.length-1);
	}
	
	private void sort(int a[], int low, int high)
	{   
		int pivotIndex = partition(a,low,high);
		if(low < pivotIndex-1)
		{
			sort(a, low, pivotIndex-1);
		}
		if(pivotIndex < high)
		{
			sort(a, pivotIndex, high);
		}
	}

	/**
	 * finds the pivot and put pivot in its correct position
	 * by moving all elements smaller than pivot to left
	 * and elements greater than pivot to right
	 * @return the index of pivot after partition
	 */
	private int partition(int[] input,int low,int high)
	{
		int i = low;
		int j = high;
		int pivot = choosePivot(input, low, high);
		while(i <= j)
		{
			while(input[i] < pivot)
				i++;
			while(input[j] > pivot)
				j--;
			if(i <= j) 
			{
				Utils.swap(input, i, j);
				i++;
				j--;
			}
		}
		return i;
	}

	/**
	 * @return the element at the high index
	 */
	private int choosePivot(int[] a, int high, int low) {
		return a[high];
	}
}