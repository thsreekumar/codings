package org.sreekumar.ds.sorting.quick;

import org.sreekumar.common.utils.Utils;

public class QuickSortTest {

	public static void main(String args[])
	{
		int []a= {12,4,2,34};

		System.out.println("Array Before Quick Sort is : ");
		Utils.print(a);

		QuickSort q=new QuickSort();
		q.sort(a);

		System.out.println();
		System.out.println("Sorted Array is : ");
		Utils.print(a);
	}	
}
