package org.sreekumar.ds.sorting.bubble;

import org.sreekumar.common.utils.Utils;

public class BubbleSortTest {
	public static void main(String args[])
	{
		BubbleSort b=new BubbleSort();
		int[] a= {32,56,43,23,78,31};
		System.out.println("Array Before Bubble Sort is : ");
		Utils.print(a);
		System.out.println("Sorted Array is : ");
		b.sort(a);	
		Utils.print(a);
	}
}

