package org.sreekumar.ds.queue;
/*Queue program Insertion,Deletion and Display methods are used*/

public class Queue 
{
	static int MAX = 1000;
	int front = -1,rear = -1;
	int array[] = new int[MAX];
	/*insert method to insert an element in queue*/
	void insert(int number)
	{
		if(rear == (MAX - 1))
		{
			System.out.println("Queue is overflow");
		}
		if(front == -1 && rear == -1)
		{
			front = 0;
			rear = 0;
		}
		else
		{
			rear++;
		}
		array[rear] = number;
	}
	/* delete method to delete the front element from queue*/
	void delete()
	{
		if(front == -1 && rear == -1)
		{
			System.out.println("Queue is empty");
		}
		else
		{
			System.out.println("The element "+array[front]+" is deleted");
			front++;
		}
	}
	/* display method to display the queue */
	void display()
	{
		if(front == -1 && rear == -1)
		{
			System.out.println("The queue is empty");
		}
		else
		{
			System.out.println("Array : ");
			for(int i = front; i < rear; i++)
			{
				System.out.println(array[i]);
			}
		}
	}
	public static void main(String args[])
	{
		Queue q = new Queue();
		q.insert(1);
		q.insert(2);
		q.insert(3);
		q.insert(4);
		q.delete();
		q.display();
	}
}
