package org.sreekumar.common.utils;

import java.util.Arrays;

public class Utils {

	public static void swap(int[] a, int k, int m) {

		int temp;
		temp = a[k];
		a[k] = a[m];
		a[m] = temp;
	}

	public static void print(int[] a) {

		System.out.println(Arrays.toString(a));
	}
	public static void binarySearch(int a[],int l,int r,int e)
	{
		if(r>=l)
		{
			int mid=((r-1)+l)/2;
			if(a[mid] == e)
				System.out.print("Element"+e+"found at"+mid);
			if(a[mid] > e)
			{
				binarySearch(a, l, (mid-1), e);
			}
			if(a[mid] < e)
			{
				binarySearch(a, (mid+1), r, e);
			}
		}
		else 
			System.out.println("Element not Found!!!");
	}
}