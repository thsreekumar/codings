package org.sreekumar.basics;

public class CountElements {
	static void countElements(int array1[], int array2[])
	{
		int size1 = array1.length;
		int size2 = array2.length;
		int count = 0;

		for(int i = 0; i < size1 - 1; i++)
		{
			for(int j = i+1; j < size1; j++)
			{
				if( array1[i] == array1[j])
				{
					count++;
				}
			}
		}
		for(int i = 0; i < size2 - 1; i++)
		{
			for(int j = i+1; j < size2; j++)
			{
				if( array2[i] == array2[j])
				{
					count++;
				}
			}
		}
		for(int i = 0; i < size1; i++)
		{
			for(int j = 0; j < size2; j++)
			{
				if(array1[i] == array2[j])
				{
					count++;
				}
			}
		}
		int totalElements = (size1 + size2) - count;
		System.out.println(+count);
		System.out.println("The Number of elements without duplication : "+totalElements);
	}
	public static void main(String args[])
	{
		int array1[] = {1, 2, 2, 3, 4, 7, 9};
		int array2[] = {0, 1, 2, 1, 1, 4};
		countElements(array1, array2);
	}
}
