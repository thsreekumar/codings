package org.sreekumar.basics;
class MergeArrays 
{
	void moveToEnd(int mPlusN[], int size) 
	{
		int i, j = size - 1;
		for (i = size - 1; i >= 0; i--) 
		{
			if (mPlusN[i] != -1) 
			{
				mPlusN[j] = mPlusN[i];
				j--;
			}
		}
	}
	void merge(int mPlusN[], int N[], int m, int n) 
	{
		int i = n;
		int j = 0;
		int k = 0;
		while (k < (m + n)) 
		{
			if ((i < (m + n) && mPlusN[i] <= N[j]) || (j == n)) 
			{
				mPlusN[k] = mPlusN[i];
				k++;
				i++;
			} 
			else 
			{
				mPlusN[k] = N[j];
				k++;
				j++;
			}
		}
	}
	void display(int arr[], int size) 
	{
		int i;
		for (i = 0; i < size; i++) 
			System.out.print(arr[i] + " ");

		System.out.println("");
	}
	public static void main(String[] args) 
	{
		MergeArrays ma = new MergeArrays();
		int mPlusN[] = {1, 2, -1, -1, 5, -1, 7, 8, -1};
		int array[] = {3, 4,6,9 };
		int n = array.length;
		int m = mPlusN.length - n;
		ma.moveToEnd(mPlusN, m + n);
		ma.merge(mPlusN, array, m, n);
		ma.display(mPlusN, m + n);
	}
}
