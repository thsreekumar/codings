package org.sreekumar.basics;
/* Program to print the numbers which contain the digit d from 0 to n */
public class FindTheNumbers {
	static boolean isDigitPresent(int x, int d)
	{
		while (x > 0)
		{
			if (x % 10 == d)
			{
				break;
			}
			x = x / 10;
		}
		return (x > 0);
	}
	// method to display the values
	static void printNumbers(int n, int d)
	{
		for (int i = 0; i <= n; i++)
		{
			if (i == d || isDigitPresent(i, d))
			{
				System.out.print(i + " ");
			}
		}
	}
	public static void main(String[] args)
	{
		int n = 27, d = 5;
		printNumbers(n, d);
	}
}
