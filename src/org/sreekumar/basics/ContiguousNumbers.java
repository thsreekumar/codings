package org.sreekumar.basics;

import java.util.Arrays;

public class ContiguousNumbers {
	static boolean contiguousNumbers(int array[])
	{
		int n = array.length;
		Arrays.sort(array);
		for(int i = 1; i < n; i++)
		{
			if(array[i] - array[i-1] > 1)
			{
				return false;
			}

		}
		return true;
	}
	public static void main(String args[])
	{
		int array[] = {2, 3, 6, 10, 4, 7, 5, 8};
		int n = array.length;
		if(contiguousNumbers(array))
		{
			System.out.println("The given array is contiguous");
		}
		else
		{
			System.out.println("Not contiguous!!!");
		}
	}
}
