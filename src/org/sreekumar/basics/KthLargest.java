package org.sreekumar.basics;

import java.util.Arrays;

import org.sreekumar.common.utils.Utils;

public class KthLargest {
	static int findKthLargest(int array[], int k)
	{
		int n = array.length;
		for (int i = 0; i < n-1; i++)
		{
			for (int j = 0; j < n-i-1; j++)
			{      
				if (array[j] > array[j+1])
				{
					Utils.swap(array,j,j+1);
				}
			}
		}
		return(array[n-k]);
	}
	public static void main(String args[])
	{
		int array[] = {7, 2, 5, 1, 2, 6};
		System.out.println("Kth largest number : "+findKthLargest(array, 2));
	}
}
