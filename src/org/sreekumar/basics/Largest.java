package org.sreekumar.basics;

import org.sreekumar.common.utils.Utils;

public class Largest {
	static void kLargest(int array[], int number)
	{
		int n = array.length;
		for(int i = 0;i < n;i++)
		{
			for(int j = i+1;j < n;j++)
			{
				if(array[i] < array[j])
				{
					Utils.swap(array, i, j);
				}
			}
		}
		for(int i = n-1;i < number;i--)
		{
			System.out.println(+array[i]+" ");
		}
	}
	public static void main(String args[])
	{
		int array[] = {1,2,3,4,5,6,7,8,9};
		kLargest(array, 3);
	}
}
