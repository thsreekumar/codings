package org.sreekumar.basics;

public class MissingNumberInArray {
	public static int inputArray(int a[])
	{
		int n = a.length;
		int total = (n+1)*(n+2)/2;
		int arraysum=0;
		for(int i=0; i<n; i++)
		{
			arraysum = arraysum + a[i];
		}
		int mnumber = total - arraysum;
		return mnumber;
	}
	public static void main(String args[])
	{
		int a[] = {1,2,3,4,5,7};

		System.out.println("The missing number : "+inputArray(a));
	}
}
