package org.sreekumar.basics;

public class PerfectNumber {
	static boolean isPerfectNumber(int number)
	{
		int sum = 0;
		for(int i = 1; i <= number / 2; i++)
		{
			if(number % i == 0)
			{
				sum = sum + i;
			}
		}
		return(sum == number);
	}
	public static void main(String args[])
	{
		if(isPerfectNumber(6))
		{
			System.out.println("The given number is perfect");
		}
		else
		{
			System.out.println("Number is not perfect");
		}
	}
}
