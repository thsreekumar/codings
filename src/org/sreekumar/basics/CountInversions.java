package org.sreekumar.basics;

public class CountInversions {
	static int countInversions(int array[])
	{
		int count = 0;
		int n = array.length;
		for(int i = 0;i < n-1; i++)
		{
			for(int j = i+1; j<n; j++)
			{
				if(array[i] > array[j])
				{
					count++;
				}
			}
		}
		return count;
	}
	public static void main(String args[])
	{
		int array[] = {2,4,1,3,5};
		int n = array.length;
		System.out.println("Count inversions : "+countInversions(array));
	}
}
