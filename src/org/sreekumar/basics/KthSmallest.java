package org.sreekumar.basics;

import org.sreekumar.common.utils.Utils;

public class KthSmallest {
	public static int kthSmallest(int array[], int k)
	{
		int n = array.length;
		for (int i = 0; i < n-1; i++)
		{
			for (int j = 0; j < n-i-1; j++)
			{      
				if (array[j] > array[j+1])
				{
					Utils.swap(array,j,j+1);
				}
			}
		}
		return(array[k-1]);
	}
	public static void main(String args[])
	{
		int array[] = {1, 3, 2, 5, 6, 4, 9, 7};
		System.out.println("Kth smallest element : "+kthSmallest(array, 3));
	}
}
