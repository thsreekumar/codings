package org.sreekumar.binarysearch;

public class BinaryPivotedSearch {
	static void binarySearch(int a[],int l,int r,int e)
	{

		while(r>=l)
		{
			int mid=((r-1)+l)/2;
			if(a[mid] == e)
				System.out.print("Position is : "+ mid);
			if(a[mid] < e && a[mid] < a[r])
			{
				binarySearch(a, 0, (mid), e);
			}
			if(a[mid] > e && a[mid] > a[r])
			{
				binarySearch(a, (mid+1), r, e);
			}
			else 
				System.out.print("Number not found");
		}
	}
	public static void main(String args[])
	{
		int a[] = {9, 10, 11, 12, 1, 2, 3, 4, 5, 6, 7, 8};
		binarySearch(a, 0, 11, 1);
	}
}


